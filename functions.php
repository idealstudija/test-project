<?php

function isLogin() {
    if(isset($_SESSION['id']) && isset($_SESSION['username'])) {
        return true;
    }
}

function getList() {
    global $con;
    $query = mysqli_query($con, "SELECT * FROM categories ORDER BY id");
    
    $tree_array = array();
    while($row = mysqli_fetch_assoc($query)) {
        $tree_array[$row['id']] = $row;
    }
    
    if(count($tree_array) > 0) {
        foreach ($tree_array as $id => &$node) {    
            if(!$node['parent_id']){
                $tree[$id] = &$node;
            } else {
                $tree_array[$node['parent_id']]['childs'][$id] = &$node;
            }
        }
        return $tree;
    } else {
        return false;
    }
}

function viewMenu($tree_array) {
    echo '<ul>';
    if(is_array($tree_array)) {
    foreach($tree_array as $key => $value) {
        echo '<li>';
        echo '<a><strong>'.$value['title'].'</strong> | '.$value['description'].'</a> | <a href="#" id="update_category" data-category="'.$value['id'].'"><span class="fa fa-edit"></span></a> | <a href="#" id="delete_category" data-category="'.$value['id'].'"><span class="fa fa-close"></span></a>';
        
        if(isset($value['childs'])){
            echo '<ul>'. viewMenu($value['childs']) .'</ul>';
        }
        
        echo '</li>';
    }
    }
    echo '</ul>';
}

function viewMenuDropdown($tree_array, $str) {
    if($tree_array['parent_id'] == 0) {
        echo '<option value="'.$tree_array['id'].'">'.$tree_array['title'].'</option>';
    } else {
        echo '<option value="'.$tree_array['id'].'">'.$str.''.$tree_array['title'].'</option>';
    }
        
    if(isset($tree_array['childs'])){
        $i = 1;
        for($j = 0; $j < $i; $j++){
            $str .= '→';
        }         
        $i++;
        
        echo viewDropdown($tree_array['childs'], $str);
    }
}

function viewDropdown($tree_array, $str){
    $menu = '';
    $str = $str;
    foreach($tree_array as $value){
        $menu .= viewMenuDropdown($value, $str);
    }
    return $menu;
}

function deleteCategoryByID($id) {
    global $con;
    $query = mysqli_query($con, "SELECT * FROM categories WHERE parent_id='".intval($id)."'");
    
    if(mysqli_num_rows($query) > 0) {
        while($row = mysqli_fetch_assoc($query)) {
            deleteCategoryByID($row['id']);
        }
    }

    mysqli_query($con, "DELETE FROM categories WHERE id='".intval($id)."'");
    return true;
}

?>