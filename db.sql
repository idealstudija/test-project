CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10)  unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` text,
  `parent_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

INSERT INTO `categories` (`id`, `title`, `description`, `parent_id`) VALUES
(1, 'FORD', 'Description FORD', 0),
(2, 'MAZDA', 'Description MAZDA', 0),
(3, 'VOLVO', 'Description VOLVO', 0),
(4, 'OPEL', 'Description OPEL', 0),
(5, 'Sedan', 'Description Sedan', 1),
(6, 'hetchback', 'Description hetchback', 5),
(7, 'Coupe', 'Description Coupe', 1),
(8, 'Fastback', 'Description Fastback', 1),
(9, 'Sedan', 'Description Sedan', 2),
(10, 'Cabriolet', 'Description Cabriolet', 2),
(11, 'universal', 'Description universal', 3);



CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

INSERT INTO `users` (`id`, `username`, `password`) VALUES (1, 'admin', 'c3284d0f94606de1fd2af172aba15bf3');