<?php
include('config.php');
include('functions.php');

if(isLogin()) {
    header("Location: categories.php");
    exit();
}

if(isset($_POST['login'])) {
    $query = mysqli_query($con, "SELECT * FROM users WHERE username='".mysqli_real_escape_string($con, $_POST['username'])."' LIMIT 1");
    $row = mysqli_fetch_assoc($query);
    
    if($row['password'] == md5(md5(trim($_POST['password'])))) {
        
        $_SESSION['username'] = $row['username'];
        $_SESSION['id'] = $row['id'];
        
        header("Location: categories.php");
        exit();
    } else {
        $error_string = 'Username or password incorrect';
    }
}

include('header.php');

?>

<div class="login-form">
    <form action="" method="post">
        <h2 class="text-center">Log in</h2>
        <div class="form-group">
            <input type="text" class="form-control" name="username" placeholder="Username" value="<?= (isset($_POST['username'])?$_POST['username']:NULL); ?>" required="required">
        </div>
        <div class="form-group">
            <input type="password" class="form-control" name="password" placeholder="Password" value="<?= (isset($_POST['password'])?$_POST['password']:NULL); ?>" required="required">
        </div>
        <?php if(isset($error_string)) { ?>
        <div class="alert alert-danger" role="alert"><?= $error_string; ?></div>
        <?php } ?>
        <div class="form-group">
            <button type="submit" name="login" class="btn btn-primary btn-block">Log in</button>
        </div>      
    </form>
</div>

<?php
include('footer.php');
?>