$(document).ready(function() {

    $("#addform").submit(function(e){
    	e.preventDefault();
    	var request_method = $(this).attr("method");
    	var form_data = $(this).serialize();
    	
        $.ajax({
    		url : 'actions/add_category.php',
    		type: request_method,
    		data : form_data
    	}).done(function(msg){
    		var $msg=JSON.parse(msg);
            $("#addModal").modal('hide');
            if(typeof $msg!=="undefined" && $msg.err===0) {
               $.toast({
                    text: $msg.text,
                    icon: 'success',
                    loader: false,
                    position: 'top-center'
                });
                setTimeout(function(){
                    location.reload();
                }, 2000);
            } else {
                $.toast({
                    text: $msg.text,
                    icon: 'error',
                    loader: false,
                    position: 'top-center'
                });
            }
    	});
    });
    
    $(document).on('click', '#update_category', function(e) {
        $.ajax({
            url: "actions/load_category.php",
            type: "post",
            data: {
                id: $(this).data('category')
            },
            success: function(msg) {
                $msg=JSON.parse(msg);
                $('#updateModal').modal('show');
                $('#updateModal input[name="title"]').val($msg.title);
                $('#updateModal textarea[name="description"]').val($msg.description);
                $('#updateModal select[name="parent_id"]').val($msg.parent_id);
                $('#updateModal input[name="id"]').val($msg.id);
            }
        });
    });
    
    $("#updateform").submit(function(e){
    	e.preventDefault();
    	var request_method = $(this).attr("method");
    	var form_data = $(this).serialize();
    	
        $.ajax({
    		url : 'actions/update_category.php',
    		type: request_method,
    		data : form_data
    	}).done(function(msg){
    		var $msg=JSON.parse(msg);
            $("#updateModal").modal('hide');
            if(typeof $msg!=="undefined" && $msg.err===0) {
               $.toast({
                    text: $msg.text,
                    icon: 'success',
                    loader: false,
                    position: 'top-center'
                });
                setTimeout(function(){
                    location.reload();
                }, 2000);
            } else {
                $.toast({
                    text: $msg.text,
                    icon: 'error',
                    loader: false,
                    position: 'top-center'
                });
            }
    	});
    });
    
    $(document).on('click', '#delete_category', function(e) {
        $.ajax({
            url: "actions/delete_category.php",
            type: "post",
            data: {
                id: $(this).data('category')
            },
            success: function(msg) {
                var $msg=JSON.parse(msg);
                if(typeof $msg!=="undefined" && $msg.err===0) {
                   $.toast({
                        text: $msg.text,
                        icon: 'success',
                        loader: false,
                        position: 'top-center'
                    });
                    setTimeout(function(){
                        location.reload();
                    }, 2000);
                } else {
                    $.toast({
                        text: $msg.text,
                        icon: 'error',
                        loader: false,
                        position: 'top-center'
                    });
                }
            }
        });
    });

});