<?php
if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    include('../config.php');
    
    $res_category = mysqli_query($con, "SELECT * FROM categories WHERE id='".mysqli_real_escape_string($con, $_POST['id'])."'");
    $row_category = mysqli_fetch_assoc($res_category);
    
    if(count($row_category) > 0) {
        $answer['err'] = 0;
        $answer['id'] = $row_category['id'];
        $answer['title'] = $row_category['title'];
        $answer['description'] = $row_category['description'];
        $answer['parent_id'] = $row_category['parent_id'];
    } else {
        $answer['err'] = 1;
        $answer['msg'] = 'Error Load Category';
    }
    
    echo json_encode($answer);
}
?>