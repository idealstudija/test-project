<?php
include('config.php');
include('functions.php');

if(!isLogin()) {
    header("Location: index.php");
    exit();
}

include('header.php');
?>

<div class="">
    <div class="float-left"><a href="#" id="add_category" class="btn btn-success" data-toggle="modal" data-target="#addModal"><span class="fa fa-plus-circle"></span>Add Category</a></div>
    <div class="float-right"><a href="logout.php" id="logout" class="btn btn-primary"><span class="fa fa-sign-out"></span>Logout</a></div>
    <div class="clearfix"></div>
</div>

<hr />

<?php
echo viewMenu(getList());
//echo '<pre>'.print_r(getList(), 1).'</pre>';
?>

<div class="modal fade" id="addModal" tabindex="-1" role="dialog" aria-labelledby="addModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="addModalLabel">Add category</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="addform" method="POST">
      <div class="modal-body">
        <div class="form-group">
            <label for="InputTitle">Title</label>
            <input type="text" name="title" class="form-control" id="InputTitle" placeholder="Title" required="true">
        </div>
        <div class="form-group">
            <label for="InputDescription">Description</label>
            <textarea name="description" class="form-control" id="InputDescription" placeholder="Description" rows="3"></textarea>
        </div>
        <div class="form-group">
            <label for="SelectParent">Parent Catgory</label>
            <select name="parent_id" class="form-control" id="SelectParent">
            <option value="0">Select Category</option>
            <?= viewDropdown(getList(), ''); ?>
            </select>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-success">Add</button>
      </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="updateModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="updateModalLabel">Update category</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="updateform" method="POST">
      <div class="modal-body">
        <div class="form-group">
            <label for="InputTitle">Title</label>
            <input type="text" name="title" class="form-control" id="InputTitle" placeholder="Title" required="true">
        </div>
        <div class="form-group">
            <label for="InputDescription">Description</label>
            <textarea name="description" class="form-control" id="InputDescription" placeholder="Description" rows="3"></textarea>
        </div>
        <div class="form-group">
            <label for="SelectParent">Parent Catgory</label>
            <select name="parent_id" class="form-control" id="SelectParent">
            <option value="0">Select Category</option>
            <?= viewDropdown(getList(), ''); ?>
            </select>
        </div>
      </div>
      <div class="modal-footer">
        <input type="hidden" name="id" value="" />
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-success">Update</button>
      </div>
      </form>
    </div>
  </div>
</div>

<?php
include('footer.php');
?>